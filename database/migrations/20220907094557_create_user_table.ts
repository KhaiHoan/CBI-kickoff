import { Knex } from "knex";

const userTableName = 'users'

export async function up(knex: Knex): Promise<void> {
    const isExisted = await knex.schema.hasTable(userTableName)

    if (isExisted) { return; }

    return knex.schema.createTable(userTableName, (table) => {
        table.uuid('id').primary()

        table.string('first_name')
        table.string('last_name')

        table.string('email')
        table.string('password')

        table.integer('status')

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.uuid('created_by', { primaryKey: false }).nullable();

        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.uuid('updated_by', { primaryKey: false }).nullable();
    })
}


export async function down(knex: Knex): Promise<void> {
    const isExisted = await knex.schema.hasTable(userTableName);

    if (!isExisted) { return; }

    return knex.schema.dropTable(userTableName)
}

