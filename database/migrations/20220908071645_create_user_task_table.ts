import { Knex } from "knex";

const taskTableName = 'tasks'

export async function up(knex: Knex): Promise<void> {
    const isExisted = await knex.schema.hasTable(taskTableName)

    if (isExisted) { return; }

    return knex.schema.createTable(taskTableName, (table) => {
        table.string('id').primary();

        table.string('title');

        table.timestamp('start_time').defaultTo(knex.fn.now());
        table.timestamp('end_time').nullable();

        table.string('description');
        table.integer('status');

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.uuid('created_by').nullable();

        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.uuid('updated_by').nullable();
    })
}


export async function down(knex: Knex): Promise<void> {
    const isExisted = await knex.schema.hasTable(taskTableName)

    if (!isExisted) { return; }

    return await knex.schema.dropTable(taskTableName);
}

