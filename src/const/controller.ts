export const CONTROLLER = {
    USER: Symbol.for('USER_CONTROLLER'),
    TASK: Symbol.for('TASK_CONTROLLER'),
    AUTH: Symbol.for('AUTH_CONTROLLER'),
}

