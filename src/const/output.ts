export const USER_OUTPUT = {
    CREATE: Symbol.for('CREATE_USER_OUTPUT'),
    UPDATE: Symbol.for('UPDATE_USER_OUTPUT'),
    GET_BY_ID: Symbol.for('GET_USER_BY_ID_OUTPUT')
}

export const TASK_OUTPUT = {
    CREATE: Symbol.for('CREATE_TASK_OUTPUT'),
    UPDATE: Symbol.for('UPDATE_TASK_OUTPUT'),
    GET_BY_ID: Symbol.for('GET_TASK_BY_ID_OUTPUT'),
    PAGINATE: Symbol.for('PAGINATE_TASK_OUTPUT'),
    DELETE: Symbol.for('DELETE_TASK_OUTPUT'),
    COUNT: Symbol.for('COUNT_TASK_OUTPUT'),
    CHANGE_TO_FINISHED: Symbol.for('CHANGE_TASK_TO_FINISHED_OUTPUT'),
}

export const AUTH_OUTPUT = {
    SIGN_IN: Symbol.for('SIGN_IN_OUTPUT')
}