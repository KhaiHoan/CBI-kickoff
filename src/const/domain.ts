export const API_DOMAIN = {
    USER: Symbol.for('USER'),
    TASK: Symbol.for('TASK'),
    USER_TASK: Symbol.for('USER_TASK')
}