export const DATABASE = {
    USER: Symbol.for('USER_POSTGRES')
}

export enum TABLE {
    USER = 'users',
    TASK = 'tasks',
    USER_TASK = 'users_tasks'
}
