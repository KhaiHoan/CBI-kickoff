export const USER_INPUT = {
    CREATE: Symbol.for('CREATE_USER_INPUT'),
    UPDATE: Symbol.for('UPDATE_USER_INPUT'),
    GET_BY_ID: Symbol.for('GET_USER_BY_ID_INPUT')
}

export const TASK_INPUT = {
    CREATE: Symbol.for('CREATE_TASK_INPUT'),
    UPDATE: Symbol.for('UPDATE_TASK_INPUT'),
    GET_BY_ID: Symbol.for('GET_TASK_BY_ID_INPUT'),
    PAGINATE: Symbol.for('PAGINATE_TASK_INPUT'),
    DELETE: Symbol.for('DELETE_TASK_INPUT'),
    COUNT: Symbol.for('COUNT_TASK_INPUT'),
    CHANGE_TO_FINISHED: Symbol.for('CHANGE_TASK_TO_FINISHED_INPUT'),
}


export const AUTH_INPUT = {
    SIGN_IN: Symbol.for('SIGN_IN_INPUT')
}