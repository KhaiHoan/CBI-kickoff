export const NAMES = {
    SERVER: Symbol.for('SERVER'),
    API: Symbol.for('API'),
    QUERY: Symbol.for('QUERY'),
    POSTGRES: Symbol.for('POSTGRES'),
    POSTGRES_CONNECTION: Symbol.for('POSTGRES_CONNECTION'),
};