export const USER_USECASE = {
    CREATE: Symbol.for('CREATE_USER_USECASE'),
    UPDATE: Symbol.for('UPDATE_USER_USECASE'),
    FIND_BY_ID: Symbol.for('FIND_USER_BY_ID_USECASE')
}

export const TASK_USECASE = {
    CREATE: Symbol.for('CREATE_TASK_USECASE'),
    UPDATE: Symbol.for('UPDATE_TASK_USECASE'),
    GET_BY_ID: Symbol.for('GET_TASK_BY_ID_USECASE'),
    PAGINATE: Symbol.for('PAGINATE_TASK_USECASE'),
    DELETE: Symbol.for('DELETE_TASK_USECASE'),
    COUNT: Symbol.for('COUNT_TASK_USECASE'),
    CHANGE_TO_FINISHED: Symbol.for('CHANGE_TASK_TO_FINISHED_USECASE'),
}

export const AUTH_USECASE = {
    SIGN_IN: Symbol.for('SIGN_IN_USECASE'),
}