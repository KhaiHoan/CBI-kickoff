import { inject, interfaces } from "inversify";
import { TYPES, CONTROLLER, USER_INPUT } from "../../const";
import { USER_OUTPUT } from "../../const/output";
import { USER_USECASE } from "../../const/usecase";
import { ControllerResult } from "../../http";
import { BaseController } from "../../infrastructure";
import { singletonNamedProvide, namedInject } from "../../infrastructure/ioc";
import { ICreateUserUsecase, ICreateUserInput, ICreateUserOutput } from "../../usecases";


export interface IuserController {
    createUser(req: Request): Promise<ControllerResult>
}

@singletonNamedProvide(TYPES.CONTROLLER, CONTROLLER.USER)
export class UserController extends BaseController implements IuserController {
    get id() {
        return CONTROLLER.USER;
    }

    @namedInject(TYPES.USECASE, USER_USECASE.CREATE)
    protected createUserUsecase: ICreateUserUsecase;

    @inject(USER_INPUT.CREATE)
    protected createUserInput: interfaces.Newable<ICreateUserInput>;
    @inject(USER_OUTPUT.CREATE)
    protected createUserOutput: interfaces.Newable<ICreateUserOutput>;

    async createUser(req: Request): Promise<ControllerResult> {
        const input = new this.createUserInput(req)

        const output = new this.createUserOutput(await this.createUserUsecase.execute(input));

        return { content: output.response }
    }
}