import { inject, interfaces } from "inversify";
import { TYPES, CONTROLLER, AUTH_INPUT, } from "../../const";
import { AUTH_OUTPUT } from "../../const/output";
import { AUTH_USECASE } from "../../const/usecase";
import { ControllerResult } from "../../http";
import { singletonNamedProvide, BaseController, namedInject } from "../../infrastructure";
import { IUserSignInUsecase } from "../../usecases";
import { IUserSignInInput } from "../../usecases/inputs/auth";
import { IUserSignInOutput } from "../../usecases/outputs/auth";

export interface IAuthController {
    signIn(req: Request): Promise<ControllerResult>
}

@singletonNamedProvide(TYPES.CONTROLLER, CONTROLLER.AUTH)
export class AuthController extends BaseController implements IAuthController {
    get id() {
        return CONTROLLER.AUTH;
    }

    @namedInject(TYPES.USECASE, AUTH_USECASE.SIGN_IN)
    protected signInUsecase: IUserSignInUsecase;

    @inject(AUTH_INPUT.SIGN_IN)
    protected signInInput: interfaces.Newable<IUserSignInInput>;
    @inject(AUTH_OUTPUT.SIGN_IN)
    protected signInOutput: interfaces.Newable<IUserSignInOutput>;

    async signIn(req: Request): Promise<ControllerResult> {
        const input = new this.signInInput(req)

        const output = new this.signInOutput(await this.signInUsecase.execute(input));

        return { content: output.response }
    }
}