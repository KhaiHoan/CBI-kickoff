import { TYPES, CONTROLLER, TASK_INPUT } from "../../const";
import { TASK_OUTPUT } from "../../const/output";
import { TASK_USECASE } from "../../const/usecase";
import { ControllerResult } from "../../http";
import { singletonNamedProvide, BaseController, namedInject, inject, interfaces } from "../../infrastructure";
import { IChangeTaskToFinishedInput, IChangeTaskToFinishedOutput, IChangeTaskToFinishedUsecase, ICountTaskInput, ICountTaskOutput, ICountTaskUsecase, ICreateTaskUsecase, IDeleteTaskUsecase, IGetTaskByIdInput, IGetTaskByIdOutput, IGetTaskByIdUsecase, IPaginateTaskInput, IPaginateTaskOutput, IPaginateTaskUsecase, IUpdateTaskUsecase } from "../../usecases";
import { ICreateTaskInput, IUpdateTaskInput, IDeleteTaskInput } from "../../usecases";
import { ICreateTaskOutput, IUpdateTaskOutput, IDeleteTaskOutput } from "../../usecases";

export interface ITaskController {
    createTask(req: Request): Promise<ControllerResult>;
    updateTask(req: Request): Promise<ControllerResult>;
    deleteTask(req: Request): Promise<ControllerResult>;
    paginateTask(req: Request): Promise<ControllerResult>;
    countTask(req: Request): Promise<ControllerResult>;
    getTaskById(req: Request): Promise<ControllerResult>;
    changeTaskToFinished(req: Request): Promise<ControllerResult>
}

@singletonNamedProvide(TYPES.CONTROLLER, CONTROLLER.TASK)
export class TaskController extends BaseController implements ITaskController {
    get id() {
        return CONTROLLER.TASK;
    }

    @namedInject(TYPES.USECASE, TASK_USECASE.CREATE)
    protected createTaskUsecase: ICreateTaskUsecase;

    @inject(TASK_INPUT.CREATE)
    protected createTaskInput: interfaces.Newable<ICreateTaskInput>;
    @inject(TASK_OUTPUT.CREATE)
    protected createTaskOutput: interfaces.Newable<ICreateTaskOutput>;

    @namedInject(TYPES.USECASE, TASK_USECASE.UPDATE)
    protected updateTaskUsecase: IUpdateTaskUsecase;

    @inject(TASK_INPUT.UPDATE)
    protected updateTaskInput: interfaces.Newable<IUpdateTaskInput>;
    @inject(TASK_OUTPUT.UPDATE)
    protected updateTaskOutput: interfaces.Newable<IUpdateTaskOutput>;

    @namedInject(TYPES.USECASE, TASK_USECASE.DELETE)
    protected deleteTaskUsecase: IDeleteTaskUsecase;

    @inject(TASK_INPUT.DELETE)
    protected deleteTaskInput: interfaces.Newable<IDeleteTaskInput>;
    @inject(TASK_OUTPUT.DELETE)
    protected deleteTaskOutput: interfaces.Newable<IDeleteTaskOutput>;

    @namedInject(TYPES.USECASE, TASK_USECASE.GET_BY_ID)
    protected getTaskByIdUsecase: IGetTaskByIdUsecase;

    @inject(TASK_INPUT.GET_BY_ID)
    protected getTaskByIdInput: interfaces.Newable<IGetTaskByIdInput>;
    @inject(TASK_OUTPUT.GET_BY_ID)
    protected getTaskByIdOutput: interfaces.Newable<IGetTaskByIdOutput>;

    @namedInject(TYPES.USECASE, TASK_USECASE.PAGINATE)
    protected paginateTaskUsecase: IPaginateTaskUsecase;

    @inject(TASK_INPUT.PAGINATE)
    protected paginateTaskInput: interfaces.Newable<IPaginateTaskInput>;
    @inject(TASK_OUTPUT.PAGINATE)
    protected paginateTaskOutput: interfaces.Newable<IPaginateTaskOutput>;

    @namedInject(TYPES.USECASE, TASK_USECASE.COUNT)
    protected countTaskUsecase: ICountTaskUsecase;

    @inject(TASK_INPUT.COUNT)
    protected countTaskInput: interfaces.Newable<ICountTaskInput>;
    @inject(TASK_OUTPUT.COUNT)
    protected countTaskOutput: interfaces.Newable<ICountTaskOutput>;

    @namedInject(TYPES.USECASE, TASK_USECASE.CHANGE_TO_FINISHED)
    protected changeTaskToFinishedUsecase: IChangeTaskToFinishedUsecase;

    @inject(TASK_INPUT.CHANGE_TO_FINISHED)
    protected changeTaskToFinishedInput: interfaces.Newable<IChangeTaskToFinishedInput>;
    @inject(TASK_OUTPUT.CHANGE_TO_FINISHED)
    protected changeTaskToFinishedOutput: interfaces.Newable<IChangeTaskToFinishedOutput>;

    async createTask(req: Request): Promise<ControllerResult> {
        const input = new this.createTaskInput(req)

        const output = new this.createTaskOutput(await this.createTaskUsecase.execute(input));

        return { content: output.response };
    }

    async updateTask(req: Request): Promise<ControllerResult> {
        const input = new this.updateTaskInput(req)

        const output = new this.updateTaskOutput(await this.updateTaskUsecase.execute(input));

        return { content: output.response };
    }

    async deleteTask(req: Request): Promise<ControllerResult> {
        const input = new this.deleteTaskInput(req)

        const output = new this.deleteTaskOutput(await this.deleteTaskUsecase.execute(input));

        return { content: output.response };
    }

    async getTaskById(req: Request): Promise<ControllerResult> {
        const input = new this.getTaskByIdInput(req)

        const output = new this.getTaskByIdOutput(await this.getTaskByIdUsecase.execute(input));

        return { content: output.response };
    }

    async paginateTask(req: Request): Promise<ControllerResult> {
        const input = new this.paginateTaskInput(req)

        const output = new this.paginateTaskOutput(await this.paginateTaskUsecase.execute(input));

        return { content: output.response };
    }

    async countTask(req: Request): Promise<ControllerResult> {
        const input = new this.countTaskInput(req)

        const output = new this.countTaskOutput(await this.countTaskUsecase.execute(input));

        return { content: output.response };
    }

    async changeTaskToFinished(req: Request): Promise<ControllerResult> {
        const input = new this.changeTaskToFinishedInput(req)

        const output = new this.changeTaskToFinishedOutput(await this.changeTaskToFinishedUsecase.execute(input));

        return { content: output.response };
    }
}