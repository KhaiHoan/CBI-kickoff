export {
    BaseApplication,
    IApplication
} from './infrastructure/base/application';
import {
    buildProviderModule,
    container
} from './infrastructure/ioc';


import './infrastructure';
import './application';
import './usecases';
import './controllers';
import './repositories';
import './database';
import './http';
import './domains';

container.load(buildProviderModule())

export { container }