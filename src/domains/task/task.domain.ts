import { BaseDomain } from "../../infrastructure/base/domain";
import { API_DOMAIN } from "../../const";
import { ITaskDomain } from ".";
import { IUserDomain } from "../user";

export class TaskDomain extends BaseDomain<ITaskDomain> implements ITaskDomain {
    get id() {
        return this.context.id;
    }

    get title() {
        return this.context.title;
    }

    get startTime() {
        return this.context.startTime;
    }

    get endTime() {
        return this.context.endTime;
    }

    get description() {
        return this.context.description;
    }

    get status() {
        return this.context.status;
    }

    get users() {
        return this.context.users;
    }

    get createdAt() {
        return this.context.createdAt;
    }

    get createdBy() {
        return this.context.createdBy;
    }

    get updatedAt() {
        return this.context.updatedAt;
    }

    get updatedBy() {
        return this.context.updatedBy;
    }

    protected nameContext = API_DOMAIN.USER;


    json(): ITaskDomain {
        const {
            id,
            title,
            startTime,
            endTime,
            description,
            status,
            createdBy,
            updatedBy,
            createdAt,
            updatedAt
        } = this;
        return {
            id,
            title,
            startTime,
            endTime,
            description,
            status,
            createdBy,
            updatedBy,
            createdAt,
            updatedAt
        };
    }

    toString() {
        return JSON.stringify(this.json());
    }
} 