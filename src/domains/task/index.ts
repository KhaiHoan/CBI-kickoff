import { TASK_STATUS } from '../../const';
import { IDomain } from '../../infrastructure';
import { IUserDomain } from '../user';

export interface ITask {
    title: string;
    startTime: Date;
    endTime: Date;
    description: string;
    status: TASK_STATUS;
    users?: IUserDomain[] | string[];
    createdBy: string;
    createdAt: Date;
    updatedBy: string;
    updatedAt: Date;
}

export interface ITaskDomain extends ITask, IDomain { }

export * from './task.domain';


