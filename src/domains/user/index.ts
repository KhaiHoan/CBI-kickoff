import { USER_STATUS } from '../../const';
import { IDomain } from '../../infrastructure';
import { ITaskDomain } from '../task';

export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    status: USER_STATUS;
    tasks?: ITaskDomain[];
    createdBy: string;
    createdAt: Date;
    updatedBy: string;
    updatedAt: Date
}

export interface IUserDomain extends IUser, IDomain { }

export * from './user.domain';
