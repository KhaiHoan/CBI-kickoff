import { IUserDomain } from "./index";
import { BaseDomain } from "../../infrastructure/base/domain";
import { API_DOMAIN } from "../../const";

export class UserDomain extends BaseDomain<IUserDomain> implements IUserDomain {
    get id() {
        return this.context.id;
    }

    get firstName() {
        return this.context.firstName;
    }

    get lastName() {
        return this.context.lastName;
    }

    get email() {
        return this.context.email;
    }

    get password() {
        return this.context.password;
    }

    get status() {
        return this.context.status;
    }

    get tasks() {
        return this.context.tasks?.length > 0 ? this.tasks : [];
    }

    get createdAt() {
        return this.context.createdAt;
    }

    get createdBy() {
        return this.context.createdBy;
    }

    get updatedAt() {
        return this.context.updatedAt;
    }

    get updatedBy() {
        return this.context.updatedBy;
    }

    protected nameContext = API_DOMAIN.USER;


    json(): IUserDomain {
        const {
            id,
            firstName,
            lastName,
            email,
            status,
            password,
            createdBy,
            updatedBy,
            createdAt,
            updatedAt
        } = this;
        return {
            id,
            firstName,
            lastName,
            email,
            status,
            password,
            createdBy,
            updatedBy,
            createdAt,
            updatedAt
        };
    }

    toString() {
        return JSON.stringify(this.json());
    }
} 