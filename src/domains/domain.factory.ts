import { API_DOMAIN, TYPES } from '../const';
import { IDomain } from '../infrastructure/base/domain';
import { singletonProvide } from '../infrastructure/ioc';

import { TaskDomain } from './task';
import { UserDomain } from './user';
import { UserTaskDomain } from './user-task';

export interface IEntityFactory {
    create(type: string, data: any): IDomain
}

@singletonProvide(TYPES.ENTITY_FACTORY)
export class EntityFactory {
    create(type: string, data: any) {
        switch (type) {
            case API_DOMAIN.USER.toString(): return new UserDomain(data).json();

            case API_DOMAIN.TASK.toString(): return new TaskDomain(data).json();

            case API_DOMAIN.USER_TASK.toString(): return new UserTaskDomain(data).json();

            default: throw new Error(`${type} is not suppported.`)
        }
    }
}