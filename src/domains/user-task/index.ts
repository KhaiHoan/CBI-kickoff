import { USER_TASK_STATUS } from '../../const';
import { IDomain } from '../../infrastructure';
import { ITaskDomain } from '../task';
import { IUserDomain } from '../user';

export interface IUserTask {
    userId: string;
    taskId: string;
    user?: IUserDomain;
    task?: ITaskDomain;
    status: USER_TASK_STATUS;
    createdBy: string;
    createdAt: Date;
    updatedBy: string;
    updatedAt: Date;
}

export interface IUserTaskDomain extends IUserTask, IDomain { }

export * from './user-task.domain';


