import { BaseDomain } from "../../infrastructure/base/domain";
import { API_DOMAIN } from "../../const";
import { IUserTaskDomain } from ".";

export class UserTaskDomain extends BaseDomain<IUserTaskDomain> implements IUserTaskDomain {
    get id() {
        return this.context.id;
    }

    get userId() {
        return this.context.userId;
    }

    get taskId() {
        return this.context.taskId;
    }

    get user() {
        return this.context.user;
    }

    get task() {
        return this.context.task;
    }

    get status() {
        return this.context.status;
    }

    get createdAt() {
        return this.context.createdAt;
    }

    get createdBy() {
        return this.context.createdBy;
    }

    get updatedAt() {
        return this.context.updatedAt;
    }

    get updatedBy() {
        return this.context.updatedBy;
    }

    protected nameContext = API_DOMAIN.TASK;


    json(): IUserTaskDomain {
        const {
            id,
            userId,
            taskId,
            user,
            task,
            status,
            createdBy,
            updatedBy,
            createdAt,
            updatedAt
        } = this;
        return {
            id,
            userId,
            taskId,
            user,
            task,
            status,
            createdBy,
            updatedBy,
            createdAt,
            updatedAt
        };
    }

    toString() {
        return JSON.stringify(this.json());
    }
} 