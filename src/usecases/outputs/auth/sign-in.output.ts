import { AUTH_OUTPUT } from "../../../const/output";
import { IBaseHttpOutput, constructorProvide, BaseHttpOutput } from "../../../infrastructure";

export interface IUserSignInOutput extends IBaseHttpOutput {
    message: string;
    token: string;
}

@constructorProvide(AUTH_OUTPUT.SIGN_IN)
export class UserSignInOutput extends BaseHttpOutput<IUserSignInOutput> implements IUserSignInOutput {
    constructor(output: IUserSignInOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get token() {
        return this.output.token;
    }

    get response() {
        const {
            message,
            token
        } = this;

        return {
            message,
            token
        }
    }
}