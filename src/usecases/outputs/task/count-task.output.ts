import { TASK_OUTPUT } from "../../../const/output";
import { ITaskDomain } from "../../../domains";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface ICountTaskOutput extends IBaseHttpOutput {
    message: string;
    total: number;
}

@constructorProvide(TASK_OUTPUT.COUNT)
export class CountTaskOutput extends BaseHttpOutput<ICountTaskOutput> implements ICountTaskOutput {
    constructor(output: ICountTaskOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get total() {
        return this.output.total
    }

    get response() {
        const {
            message,
            total
        } = this;

        return {
            message,
            total
        }
    }
}