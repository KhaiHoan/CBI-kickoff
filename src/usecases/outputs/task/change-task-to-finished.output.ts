import { TASK_OUTPUT } from "../../../const/output";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IChangeTaskToFinishedOutput extends IBaseHttpOutput {
    message: string;
}

@constructorProvide(TASK_OUTPUT.CHANGE_TO_FINISHED)
export class ChangeTaskToFinishedOutput extends BaseHttpOutput<IChangeTaskToFinishedOutput> implements IChangeTaskToFinishedOutput {
    constructor(output: IChangeTaskToFinishedOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get response() {
        const {
            message
        } = this;

        return {
            message
        }
    }
}