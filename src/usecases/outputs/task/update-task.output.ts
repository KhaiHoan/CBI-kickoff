import { TASK_OUTPUT } from "../../../const/output";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IUpdateTaskOutput extends IBaseHttpOutput {
    message: string;
}

@constructorProvide(TASK_OUTPUT.UPDATE)
export class UpdateTaskOutput extends BaseHttpOutput<IUpdateTaskOutput> implements IUpdateTaskOutput {
    constructor(output: IUpdateTaskOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get response() {
        const {
            message
        } = this;

        return {
            message
        }
    }
}