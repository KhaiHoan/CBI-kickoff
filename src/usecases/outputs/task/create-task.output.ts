import { TASK_OUTPUT } from "../../../const/output";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface ICreateTaskOutput extends IBaseHttpOutput {
    message: string;
}

@constructorProvide(TASK_OUTPUT.CREATE)
export class CreateTaskOutput extends BaseHttpOutput<ICreateTaskOutput> implements ICreateTaskOutput {
    constructor(output: ICreateTaskOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get response() {
        const {
            message
        } = this;

        return {
            message
        }
    }
}