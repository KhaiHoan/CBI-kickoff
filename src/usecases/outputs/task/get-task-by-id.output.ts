import { TASK_OUTPUT } from "../../../const/output";
import { ITaskDomain } from "../../../domains";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IGetTaskByIdOutput extends IBaseHttpOutput {
    message: string;
    entity: ITaskDomain;
}

@constructorProvide(TASK_OUTPUT.GET_BY_ID)
export class GetTaskByIdOutput extends BaseHttpOutput<IGetTaskByIdOutput> implements IGetTaskByIdOutput {
    constructor(output: IGetTaskByIdOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get entity() {
        return this.output.entity
    }

    get response() {
        const {
            message,
            entity
        } = this;

        return {
            message,
            entity
        }
    }
}