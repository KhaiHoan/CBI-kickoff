import { TASK_OUTPUT } from "../../../const/output";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IDeleteTaskOutput extends IBaseHttpOutput {
    message: string;
}

@constructorProvide(TASK_OUTPUT.DELETE)
export class DeleteTaskOutput extends BaseHttpOutput<IDeleteTaskOutput> implements IDeleteTaskOutput {
    constructor(output: IDeleteTaskOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get response() {
        const {
            message
        } = this;

        return {
            message
        }
    }
}