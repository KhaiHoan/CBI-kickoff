import { TASK_OUTPUT } from "../../../const/output";
import { ITaskDomain } from "../../../domains";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IPaginateTaskOutput extends IBaseHttpOutput {
    message: string;
    entities: ITaskDomain;
}

@constructorProvide(TASK_OUTPUT.PAGINATE)
export class PaginateTaskOutput extends BaseHttpOutput<IPaginateTaskOutput> implements IPaginateTaskOutput {
    constructor(output: IPaginateTaskOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get entities() {
        return this.output.entities;
    }

    get response() {
        const {
            message,
            entities
        } = this;

        return {
            message,
            entities
        }
    }
}