import { USER_OUTPUT } from "../../../const/output";
import { IUserDomain } from "../../../domains";
import { BaseHttpOutput, IBaseHttpOutput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface ICreateUserOutput extends IBaseHttpOutput {
    message: string;
}

@constructorProvide(USER_OUTPUT.CREATE)
export class CreateUserOutput extends BaseHttpOutput<ICreateUserOutput> implements ICreateUserOutput {
    constructor(output: ICreateUserOutput) {
        super(output)
    }

    get message() {
        return this.output.message;
    }

    get response() {
        const {
            message
        } = this;

        return {
            message
        }
    }
}