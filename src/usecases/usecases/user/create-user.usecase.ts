
import { TYPES, API_DOMAIN } from "../../../const";
import { USER_USECASE } from "../../../const/usecase";
import { IEntityFactory, IUserDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject, inject } from "../../../infrastructure/ioc";
import { IUserRepository } from "../../../repositories";
import { ICreateUserInput } from "../../inputs";
import { ICreateUserOutput } from "../../outputs";

export interface ICreateUserUsecase extends IBaseUsecase<ICreateUserInput, ICreateUserOutput> { }

@singletonNamedProvide(TYPES.USECASE, USER_USECASE.CREATE)
export class CreateUserUsecase extends BaseUsecase<ICreateUserInput, ICreateUserOutput> {
    get DOMAIN() {
        return API_DOMAIN.USER;
    }

    get id() {
        return USER_USECASE.CREATE;
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER)
    protected userRepository: IUserRepository;

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context

    async validate(entity: IUserDomain): Promise<void> {
        const isExistedUser = await this.userRepository.findByEmail(entity.email)

        if (isExistedUser) {
            throw this.errorFactory.badRequestError(`This email: ${entity.email} is existed.`)
        }
    }

    async execute(input: ICreateUserInput): Promise<ICreateUserOutput> {
        const entity = <IUserDomain>this.entityFactory.create(this.DOMAIN.toString(), input)

        await this.validate(entity)

        await this.userRepository.create(entity, this.context.transaction);

        const output = <ICreateUserOutput>{
            data: {
                message: `Create user successfully`,
            }
        }

        return output.data;
    }
}