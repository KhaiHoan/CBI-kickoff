import { inject } from "inversify";
import { TYPES, API_DOMAIN, USER_TASK_STATUSES } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory, ITaskDomain, IUserTaskDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject } from "../../../infrastructure/ioc";
import { ITaskRepository, IUserTaskRepository } from "../../../repositories";

import { ICreateTaskInput } from "../../inputs";

import { ICreateTaskOutput } from "../../outputs";


export interface ICreateTaskUsecase extends IBaseUsecase<ICreateTaskInput, ICreateTaskOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.CREATE)
export class CreateTaskUsecase extends BaseUsecase<ICreateTaskInput, ICreateTaskOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.CREATE
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
    protected userTaskRepository: IUserTaskRepository

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context

    async execute(input: ICreateTaskInput): Promise<ICreateTaskOutput> {
        const task = <ITaskDomain>this.entityFactory.create(API_DOMAIN.TASK.toString(), input);

        const createdTask = await this.taskRepository.create(task)

        const userTask = <IUserTaskDomain>this.entityFactory.create(API_DOMAIN.USER_TASK.toString(), {
            userId: this.context.user.id,
            taskId: createdTask.id,
            status: USER_TASK_STATUSES.ACTIVE
        })

        await this.userTaskRepository.create(userTask)

        const output = <ICreateTaskOutput>{
            data: {
                message: `Create task successfully`,
            }
        }

        return output.data;
    }
}