import { TYPES, API_DOMAIN, USER_TASK_STATUSES } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory, ITaskDomain, IUserTaskDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject, inject } from "../../../infrastructure/ioc";
import { ITaskRepository, IUserTaskRepository } from "../../../repositories";

import { ICountTaskInput } from "../../inputs";
import { ICountTaskOutput } from "../../outputs";


export interface ICountTaskUsecase extends IBaseUsecase<ICountTaskInput, ICountTaskOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.COUNT)
export class CountTaskUsecase extends BaseUsecase<ICountTaskInput, ICountTaskOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.COUNT
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
    protected userTaskRepository: IUserTaskRepository

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context

    async execute(input: ICountTaskInput): Promise<ICountTaskOutput> {
        const count = await this.userTaskRepository.countByUserId(input.userId)

        const output = <ICountTaskOutput>{
            data: {
                message: `Count task successfully`,
                total: count
            }
        }

        return output.data;
    }
}