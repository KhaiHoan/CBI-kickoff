import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory, ITaskDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject } from "../../../infrastructure/ioc";
import { ITaskRepository } from "../../../repositories";

import { IUpdateTaskInput } from "../../inputs";
import { IUpdateTaskOutput } from "../../outputs";

export interface IUpdateTaskUsecase extends IBaseUsecase<IUpdateTaskInput, IUpdateTaskOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.UPDATE)
export class UpdateTaskUsecase extends BaseUsecase<IUpdateTaskInput, IUpdateTaskOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.UPDATE;
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository;

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    async execute(input: IUpdateTaskInput): Promise<IUpdateTaskOutput> {
        const isExisted = await this.taskRepository.findById(input.id)

        if (!isExisted) {
            throw this.errorFactory.badRequestError(`This task Id is not existed`);
        }

        if (isExisted.createdBy !== this.context.user.id) {
            throw this.errorFactory.unauthorizedError(`This task is not belong to this user`);
        }

        const payload = <ITaskDomain>this.entityFactory.create(API_DOMAIN.TASK.toString(), input);

        await this.taskRepository.updateById(isExisted.id, payload, this.context.transaction)

        const output = <IUpdateTaskOutput>{
            data: {
                message: `update task successfully`,
            }
        }

        return output.data;
    }
}