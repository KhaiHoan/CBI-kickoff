import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory, ITaskDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject } from "../../../infrastructure/ioc";
import { ITaskRepository, IUserTaskRepository } from "../../../repositories";
import { IDeleteTaskInput, IUpdateTaskInput } from "../../inputs";
import { IDeleteTaskOutput, IUpdateTaskOutput } from "../../outputs";

export interface IDeleteTaskUsecase extends IBaseUsecase<IDeleteTaskInput, IDeleteTaskOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.DELETE)
export class DeleteTaskUsecase extends BaseUsecase<IDeleteTaskInput, IDeleteTaskOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.DELETE
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
    protected userTaskRepository: IUserTaskRepository;

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    async execute(input: IDeleteTaskInput): Promise<IDeleteTaskOutput> {
        const isExisted = await this.taskRepository.findById(input.id)

        if (!isExisted) {
            throw this.errorFactory.badRequestError(`This task Id is not existed`);
        }

        if (isExisted.createdBy !== this.context.user.id) {
            throw this.errorFactory.unauthorizedError(`This task is not belong to this user`);
        }

        await this.taskRepository.deleteById(isExisted.id, this.context.transaction)

        await this.userTaskRepository.deleteByTaskId(isExisted.id, this.context.transaction)

        const output = <IDeleteTaskOutput>{
            data: {
                message: `delete task successfully`,
            }
        }

        return output.data;
    }
}