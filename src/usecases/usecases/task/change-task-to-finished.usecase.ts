import { inject } from "inversify";
import { TYPES, API_DOMAIN, TASK_STATUSES } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory, ITaskDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject } from "../../../infrastructure/ioc";
import { ITaskRepository, IUserTaskRepository } from "../../../repositories";

import { IChangeTaskToFinishedInput } from "../../inputs";
import { IChangeTaskToFinishedOutput } from "../../outputs";

export interface IChangeTaskToFinishedUsecase extends IBaseUsecase<IChangeTaskToFinishedInput, IChangeTaskToFinishedOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.CHANGE_TO_FINISHED)
export class ChangeTaskToFinishedUsecase extends BaseUsecase<IChangeTaskToFinishedInput, IChangeTaskToFinishedOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.CHANGE_TO_FINISHED;
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
    protected userTaskRepository: IUserTaskRepository

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    async execute(input: IChangeTaskToFinishedInput): Promise<IChangeTaskToFinishedOutput> {
        const isExisted = await this.taskRepository.findById(input.id)

        if (!isExisted) {
            throw this.errorFactory.badRequestError(`This task Id is not existed`);
        }

        const userTasks = await this.userTaskRepository.findByTaskId(isExisted.id);

        if (!userTasks.some((userTask) => userTask.userId === this.context.user.id)) {
            throw this.errorFactory.unauthorizedError(`This task is not belong to this user`);
        }

        if (isExisted.status === TASK_STATUSES.FINISHED) {
            throw this.errorFactory.badRequestError(`This task Id is already finished`);
        }

        const payload = <ITaskDomain>this.entityFactory.create(API_DOMAIN.TASK.toString(), {
            status: input.status,
            updatedBy: input.updatedBy
        });

        await this.taskRepository.updateById(isExisted.id, payload, this.context.transaction)

        const output = <IChangeTaskToFinishedOutput>{
            data: {
                message: `change task to finished successfully`,
            }
        }

        return output.data;
    }
}