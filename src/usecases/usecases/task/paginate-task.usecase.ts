import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject } from "../../../infrastructure/ioc";
import { ITaskRepository, IUserTaskRepository } from "../../../repositories";

import { IPaginateTaskInput } from "../../inputs";

import { IPaginateTaskOutput } from "../../outputs";


export interface IPaginateTaskUsecase extends IBaseUsecase<IPaginateTaskInput, IPaginateTaskOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.PAGINATE)
export class PaginateTaskUsecase extends BaseUsecase<IPaginateTaskInput, IPaginateTaskOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.PAGINATE
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
    protected userTaskRepository: IUserTaskRepository

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    async execute(input: IPaginateTaskInput): Promise<IPaginateTaskOutput> {
        const filterUser = [{ key: 'created_by', value: this.context.user.id }];

        const order = [{ column: input.sort, order: input.order }]

        const filter = !!input.key ? [...filterUser, { key: input.key, value: input.value }] : filterUser;

        const entities = await this.taskRepository.paginate({
            limit: input.limit,
            offset: input.offset,
            order: order,
            filter: filter
        })

        const output = <IPaginateTaskOutput>{
            data: {
                message: `paginate task successfully`,
                entities
            }
        }

        return output.data;
    }
}