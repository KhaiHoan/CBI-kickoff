import { inject } from "inversify";
import { TYPES, API_DOMAIN, USER_TASK_STATUSES } from "../../../const";
import { TASK_USECASE } from "../../../const/usecase";
import { IEntityFactory, ITaskDomain, IUserTaskDomain } from "../../../domains";
import { BaseUsecase, Context, IBaseUsecase, IErrorFactory } from "../../../infrastructure";
import { singletonNamedProvide, namedInject, lazyInject } from "../../../infrastructure/ioc";
import { ITaskRepository, IUserRepository, IUserTaskRepository } from "../../../repositories";

import { IGetTaskByIdInput } from "../../inputs";

import { IGetTaskByIdOutput } from "../../outputs";


export interface IGetTaskByIdUsecase extends IBaseUsecase<IGetTaskByIdInput, IGetTaskByIdOutput> { }

@singletonNamedProvide(TYPES.USECASE, TASK_USECASE.GET_BY_ID)
export class GetTaskByIdUsecase extends BaseUsecase<IGetTaskByIdInput, IGetTaskByIdOutput> {
    get DOMAIN() {
        return API_DOMAIN.TASK;
    }

    get id() {
        return TASK_USECASE.GET_BY_ID
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.TASK)
    protected taskRepository: ITaskRepository

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
    protected userTaskRepository: IUserTaskRepository

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER)
    protected userRepository: IUserRepository

    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context

    async execute(input: IGetTaskByIdInput): Promise<IGetTaskByIdOutput> {
        const task = <ITaskDomain>this.entityFactory.create(API_DOMAIN.TASK.toString(), input);

        const isExisted = await this.taskRepository.findById(task.id);
        if (!isExisted) {
            throw this.errorFactory.badRequestError(`Task Id is not existed`)
        }

        const userTasks = await this.userTaskRepository.findByTaskId(isExisted.id);

        if (!userTasks.some((userTask) => userTask.userId === this.context.user.id)) {
            throw this.errorFactory.unauthorizedError(`This task is not belong to this user`);
        }

        const users = userTasks.map(async (userTask) => {
            return this.userRepository.findById(userTask.userId)
        })

        isExisted.users = await Promise.all(users)

        const output = <IGetTaskByIdOutput>{
            data: {
                message: `Get Task By Id successfully`,
                entity: isExisted
            }
        }

        return output.data;
    }
}