import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../../const";
import { AUTH_USECASE, USER_USECASE } from "../../../const/usecase";
import { IEntityFactory, IUserDomain } from "../../../domains";
import { IBaseUsecase, singletonNamedProvide, BaseUsecase, IErrorFactory, namedInject, IPasswordHelper, IJwtHelper } from "../../../infrastructure";
import { IUserRepository } from "../../../repositories";
import { IUserSignInInput } from "../../inputs/auth/sign-in.input";
import { IUserSignInOutput } from "../../outputs/auth/sign-in.output";

export interface IUserSignInUsecase extends IBaseUsecase<IUserSignInInput, IUserSignInOutput> { }

@singletonNamedProvide(TYPES.USECASE, AUTH_USECASE.SIGN_IN)
export class UserSignInUsecase extends BaseUsecase<IUserSignInInput, IUserSignInOutput> {
    get DOMAIN() {
        return API_DOMAIN.USER;
    }

    get id() {
        return AUTH_USECASE.SIGN_IN;
    }

    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER)
    protected userRepository: IUserRepository;

    @inject(TYPES.PASSWORD_HELPER)
    protected passwordHelper: IPasswordHelper;

    @inject(TYPES.JWT_HELPER)
    protected jwtHelper: IJwtHelper;

    async execute(input: IUserSignInInput): Promise<IUserSignInOutput> {
        const entity = <IUserDomain>this.entityFactory.create(this.DOMAIN.toString(), input)

        const isExistedUser = await this.userRepository.findByEmail(entity.email)

        if (!isExistedUser) {
            throw this.errorFactory.badRequestError(`This email: ${entity.email} is not existed.`)
        }

        if (!this.passwordHelper.compare(entity.password, isExistedUser.password)) {
            throw this.errorFactory.badRequestError(`This password is not matching`)
        }

        const token = this.jwtHelper.signin(isExistedUser)

        const output = <IUserSignInOutput>{
            data: {
                message: `User sign in successfully`,
                token: token
            }
        }

        return output.data;
    }
}