import { AUTH_INPUT } from "../../../const";
import { Request } from "express";
import { IBaseHttpInput, constructorProvide, BaseHttpInput } from "../../../infrastructure";

export interface IUserSignInInput extends IBaseHttpInput {
    email: string;
    password: string;
}

@constructorProvide(AUTH_INPUT.SIGN_IN)
export class UserSignInInput extends BaseHttpInput<IUserSignInInput> implements IUserSignInInput {
    constructor(req: Request) {
        super(req)
    }

    get email() {
        return this.input.email;
    }

    get password() {
        return this.input.password;
    }
}