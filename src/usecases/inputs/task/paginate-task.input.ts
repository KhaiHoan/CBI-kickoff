import { Request } from "express";
import { TASK_INPUT } from "../../../const";
import { BaseHttpInput, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IPaginateTaskInput extends IBaseHttpInput {
    key?: string,
    value?: any,
    limit?: number,
    sort?: string,
    order?: string,
    offset?: number
}

@constructorProvide(TASK_INPUT.PAGINATE)
export class PaginateTaskInput extends BaseHttpInput<IPaginateTaskInput> implements IPaginateTaskInput {
    constructor(req: Request) {
        super(req)
    }

    get key() {
        return this.input.key || null;
    }

    get value() {
        return !!this.input.value && !!this.input.key ? this.input.value : null;
    }

    get sort() {
        return this.input.order ? this.input.order : 'created_at';
    }

    get order() {
        return this.input.order === 'desc' ? 'desc' : 'asc';
    }

    get limit() {
        return Number(this.input.limit) || null;
    }

    get offset() {
        return Number(this.input.offset) || 0;
    }
}