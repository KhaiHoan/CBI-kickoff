import { Request } from "express";
import { TASK_INPUT, TASK_STATUSES, TYPES } from "../../../const";
import { BaseHttpInput, Context, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide, lazyInject } from "../../../infrastructure/ioc";

export interface IUpdateTaskInput extends IBaseHttpInput {
    id?: string;
    title?: string;
    startTime?: Date;
    endTime?: Date;
    description?: Date;
    updatedAt: Date;
    updatedBy: string;
}

@constructorProvide(TASK_INPUT.UPDATE)
export class UpdateTaskInput extends BaseHttpInput<IUpdateTaskInput> implements IUpdateTaskInput {
    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    constructor(req: Request) {
        super(req)
    }
    get id() {
        return this.input.id;
    }

    get title() {
        return this.input.title;
    }

    get startTime() {
        return this.input.startTime;
    }

    get endTime() {
        return this.input.endTime;
    }

    get description() {
        return this.input.description;
    }

    get updatedAt() {
        return new Date();
    }

    get updatedBy() {
        return this.context.user.id ?? undefined;
    }
}