import { Request } from "express";
import { TASK_INPUT, TYPES } from "../../../const";
import { BaseHttpInput, Context, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide, lazyInject } from "../../../infrastructure/ioc";

export interface ICountTaskInput extends IBaseHttpInput {
    userId: string;
}

@constructorProvide(TASK_INPUT.COUNT)
export class CountTaskInput extends BaseHttpInput<ICountTaskInput> implements ICountTaskInput {
    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    constructor(req: Request) {
        super(req)
    }

    get userId() {
        return this.context.user.id;
    }
}