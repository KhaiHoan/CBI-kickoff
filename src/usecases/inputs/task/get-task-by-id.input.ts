import { Request } from "express";
import { TASK_INPUT } from "../../../const";
import { BaseHttpInput, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide } from "../../../infrastructure/ioc";

export interface IGetTaskByIdInput extends IBaseHttpInput {
    id: string
}

@constructorProvide(TASK_INPUT.GET_BY_ID)
export class GetTaskByIdInput extends BaseHttpInput<IGetTaskByIdInput> implements IGetTaskByIdInput {
    constructor(req: Request) {
        super(req)
    }

    get id() {
        return this.input.id;
    }
}