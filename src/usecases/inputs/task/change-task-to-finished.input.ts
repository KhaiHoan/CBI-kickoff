import { Request } from "express";
import { TASK_INPUT, TASK_STATUSES, TYPES } from "../../../const";
import { BaseHttpInput, Context, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide, lazyInject } from "../../../infrastructure/ioc";

export interface IChangeTaskToFinishedInput extends IBaseHttpInput {
    id: string;
    userId: string;
    status: number;
    updatedBy: string;
    updatedAt: Date;
}

@constructorProvide(TASK_INPUT.CHANGE_TO_FINISHED)
export class ChangeTaskToFinishedInput extends BaseHttpInput<IChangeTaskToFinishedInput> implements IChangeTaskToFinishedInput {
    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    constructor(req: Request) {
        super(req)
    }

    get id() {
        return this.input.id;
    }

    get userId() {
        return this.context.user.id;
    }

    get status() {
        return TASK_STATUSES.FINISHED;
    }

    get updatedBy() {
        return this.context.user.id;
    }

    get updatedAt() {
        return new Date();
    }
}