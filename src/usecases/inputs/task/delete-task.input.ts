import { Request } from "express";
import { TASK_INPUT, TYPES } from "../../../const";
import { BaseHttpInput, Context, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide, lazyInject } from "../../../infrastructure/ioc";

export interface IDeleteTaskInput extends IBaseHttpInput {
    id: string;
    updatedAt: Date;
    updatedBy: string;
}

@constructorProvide(TASK_INPUT.DELETE)
export class DeleteTaskInput extends BaseHttpInput<IDeleteTaskInput> implements IDeleteTaskInput {
    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    constructor(req: Request) {
        super(req)
    }

    get id() {
        return this.input.id;
    }

    get updatedAt() {
        return new Date();
    }

    get updatedBy() {
        return this.context.user.id ?? undefined;
    }
}