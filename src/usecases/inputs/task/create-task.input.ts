import { Request } from "express";
import { TASK_INPUT, TASK_STATUSES, TYPES } from "../../../const";
import { ITask } from "../../../domains";
import { BaseHttpInput, Context, IBaseHttpInput } from "../../../infrastructure";
import { constructorProvide, lazyInject } from "../../../infrastructure/ioc";

export interface ICreateTaskInput extends IBaseHttpInput, ITask { }

@constructorProvide(TASK_INPUT.CREATE)
export class CreateTaskInput extends BaseHttpInput<ICreateTaskInput> implements ICreateTaskInput {
    @lazyInject(TYPES.HTTP_CONTEXT)
    protected context: Context;

    constructor(req: Request) {
        super(req)
    }

    get title() {
        return this.input.title;
    }

    get startTime() {
        return new Date(this.input.startTime);
    }

    get endTime() {
        return new Date(this.input.endTime);
    }

    get description() {
        return this.input.description;
    }

    get status() {
        return TASK_STATUSES.IN_GROGRESS;
    }

    get createdAt() {
        return new Date();
    }

    get createdBy() {
        return this.context.user.id || null;
    }

    get updatedAt() {
        return new Date();
    }

    get updatedBy() {
        return this.context.user.id || undefined;
    }
}