import { Request } from "express";
import { TYPES, USER_INPUT, USER_STATUS, USER_STATUSES } from "../../../const";
import { IUser, IUserDomain } from "../../../domains";
import { BaseHttpInput, IBaseHttpInput, IUUIdHelper } from "../../../infrastructure";
import { constructorProvide, inject } from "../../../infrastructure/ioc";


export interface ICreateUserInput extends IBaseHttpInput, IUserDomain { }

@constructorProvide(USER_INPUT.CREATE)
export class CreateUserInput extends BaseHttpInput<ICreateUserInput> implements ICreateUserInput {
    constructor(
        req: Request
    ) {
        super(req)
    }

    get firstName() {
        return this.input.firstName;
    }

    get lastName() {
        return this.input.lastName;
    }

    get email() {
        return this.input.email;
    }

    get password() {
        return this.input.password;
    }

    get status() {
        return USER_STATUSES.ACTIVE;
    }

    get createdAt() {
        return new Date();
    }

    get createdBy() {
        return this.input.createdBy || null;
    }

    get updatedAt() {
        return new Date();
    }

    get updatedBy() {
        return this.input.updatedBy || null;
    }
}