import { TASK_STATUSES, TYPES } from "../../../const";
import { ISchema } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";


@singletonProvide(TYPES.SCHEMA)
export class CreateTaskInputSchema implements ISchema {
    $id = 'POST::/apis/tasks';
    title = 'task.create-task';
    type = 'object';
    required = ['title', 'description', 'authorization'];
    properties = {
        title: { type: 'string', minLength: 3, maxLength: 255 },
        description: { type: 'string', minLength: 3 },
        startTime: { type: 'string' },
        endTime: { type: 'string' },
        authorization: { type: 'string' }
    }
}

@singletonProvide(TYPES.SCHEMA)
export class UpdateTaskInputSchema implements ISchema {
    $id = 'PATCH::/apis/tasks/:id';
    title = 'task.update-task';
    type = 'object';
    required = ['id', 'authorization'];
    properties = {
        id: { type: 'string', minLength: 3, maxLength: 255 },
        title: { type: 'string', minLength: 3, maxLength: 255 },
        description: { type: 'string', minLength: 3 },
        startTime: { type: 'string' },
        endTime: { type: 'string' },
        authorization: { type: 'string' }
    }
}

@singletonProvide(TYPES.SCHEMA)
export class DeleteTaskInputSchema implements ISchema {
    $id = 'DELETE::/apis/tasks/:id';
    title = 'task.delete-task';
    type = 'object';
    required = ['id', 'authorization'];
    properties = {
        id: { type: 'string', minLength: 3, maxLength: 255 },
        authorization: { type: 'string' }
    }
}

@singletonProvide(TYPES.SCHEMA)
export class GetTaskByIdInputSchema implements ISchema {
    $id = 'GET::/apis/tasks/:id';
    title = 'task.get-task-by-id';
    type = 'object';
    required = ['id', 'authorization'];
    properties = {
        id: { type: 'string', minLength: 3, maxLength: 255 },
        authorization: { type: 'string' }
    }
}

@singletonProvide(TYPES.SCHEMA)
export class PaginateTaskInputSchema implements ISchema {
    $id = 'GET::/apis/tasks';
    title = 'task.paginate-tasks';
    type = 'object';
    required = ['authorization'];
    properties = {
        authorization: { type: 'string' },
        limit: { type: 'string' },
        offset: { type: 'string' },
        key: { type: 'string' },
        value: { type: 'string' }
    }
}

@singletonProvide(TYPES.SCHEMA)
export class CountTaskInputSchema implements ISchema {
    $id = 'GET::/apis/tasks/count';
    title = 'task.count-tasks';
    type = 'object';
    required = ['authorization'];
    properties = {
        authorization: { type: 'string' }
    }
}

@singletonProvide(TYPES.SCHEMA)
export class ChangeTaskToFinishedInputSchema implements ISchema {
    $id = 'PATCH::/apis/tasks/:id/finished';
    title = 'task.change-task-to-finished';
    type = 'object';
    required = ['id', 'authorization'];
    properties = {
        id: { type: 'string', minLength: 3, maxLength: 255 },
        authorization: { type: 'string' }
    }
}