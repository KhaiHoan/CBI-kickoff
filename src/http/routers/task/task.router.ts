import { inject } from "inversify";
import { TYPES } from "../../../const";
import { CONTROLLER } from "../../../const";
import { ILogger, IRouterValidator } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";
import { IInterceptor } from "../../middlewares";
import { ApiRouter } from "../api";


@singletonProvide(TYPES.ROUTER)
export class TaskRouter extends ApiRouter {
    get controller() {
        return CONTROLLER.TASK
    }

    constructor(
        @inject(TYPES.INTERCEPTOR)
        protected interceptor: IInterceptor,
        @inject(TYPES.LOGGER)
        logger: ILogger,
        @inject(TYPES.ROUTER_VALIDATOR)
        validator: IRouterValidator,
    ) {
        super(logger, validator);
        this.stack.push(TaskRouter.prefix);
        this.log.info(this.path)

        this.createTask();
        this.changeTaskToFinished();
        this.countTask();
        this.updateTask();
        this.deleteTask();
        this.paginateTask();
        this.getTaskById();

    }

    static get prefix() {
        return 'tasks';
    }

    // @ts-ignore
    get id() {
        return Symbol.for('TASK_ROUTER');
    }

    private createTask() {
        this.router.post(`${this.path}`, this.validator.validate, this.interceptor.intercept(this.controller, `createTask`))
    }

    private updateTask() {
        this.router.patch(`${this.path}/:id`, this.validator.validate, this.interceptor.intercept(this.controller, `updateTask`))
    }

    private deleteTask() {
        this.router.delete(`${this.path}/:id`, this.validator.validate, this.interceptor.intercept(this.controller, `deleteTask`))
    }

    private paginateTask() {
        this.router.get(`${this.path}`, this.validator.validate, this.interceptor.intercept(this.controller, `paginateTask`))
    }

    private getTaskById() {
        this.router.get(`${this.path}/:id`, this.validator.validate, this.interceptor.intercept(this.controller, `getTaskById`))
    }

    private countTask() {
        this.router.get(`${this.path}/count`, this.validator.validate, this.interceptor.intercept(this.controller, `countTask`))
    }

    private changeTaskToFinished() {
        this.router.patch(`${this.path}/:id/finished`, this.validator.validate, this.interceptor.intercept(this.controller, `changeTaskToFinished`))
    }
}
