import { TYPES } from "../../../const";
import { ISchema } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";


@singletonProvide(TYPES.SCHEMA)
export class CreateUserInputSchema implements ISchema {
    $id = 'POST::/apis/users';
    title = 'user.create-user';
    type = 'object';
    required = ['firstName', 'lastName', 'email', 'password'];
    properties = {
        fistName: { type: 'string', minLength: 3, maxLength: 255 },
        lastName: { type: 'string', minLength: 3 },
        password: { type: 'string', minLength: 3 },
        email: { type: 'string', format: 'email', minLength: 3, maxLength: 255 },
    }
}