import { inject } from "inversify";
import { TYPES } from "../../../const";
import { CONTROLLER } from "../../../const";
import { ILogger, IRouterValidator } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";
import { IInterceptor } from "../../middlewares";
import { ApiRouter } from "../api";


@singletonProvide(TYPES.ROUTER)
export class UserRouter extends ApiRouter {
    get controller() {
        return CONTROLLER.USER
    }

    constructor(
        @inject(TYPES.INTERCEPTOR)
        protected interceptor: IInterceptor,
        @inject(TYPES.LOGGER)
        logger: ILogger,
        @inject(TYPES.ROUTER_VALIDATOR)
        validator: IRouterValidator,
    ) {
        super(logger, validator);
        this.stack.push(UserRouter.prefix);
        this.log.info(this.path)

        this.helloWorld()

        this.createUser();
    }

    static get prefix() {
        return 'users';
    }

    // @ts-ignore
    get id() {
        return Symbol.for('USER_ROUTER');
    }

    private createUser() {
        this.router.post(`${this.path}`, this.validator.validate, this.interceptor.intercept(this.controller, `createUser`))
    }

    private helloWorld() {
        this.router.get(`${this.path}/hello-world`, (req, res, next) => { res.json({ data: 'hello' }) })
    }
}
