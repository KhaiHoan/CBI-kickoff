import { TYPES } from "../../../const";
import { ISchema } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";


@singletonProvide(TYPES.SCHEMA)
export class SignInInputSchema implements ISchema {
    $id = 'POST::/apis/auth/sign-in';
    title = 'auth.sign-in';
    type = 'object';
    required = ['email', 'password'];
    properties = {
        email: { type: 'string', format: 'email', minLength: 3, maxLength: 255 },
        password: { type: 'string' }
    }
}