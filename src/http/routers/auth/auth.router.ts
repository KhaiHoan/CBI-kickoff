import { inject } from "inversify";
import { TYPES } from "../../../const";
import { CONTROLLER } from "../../../const";
import { ILogger, IRouterValidator } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";
import { IInterceptor } from "../../middlewares";
import { ApiRouter } from "../api";


@singletonProvide(TYPES.ROUTER)
export class AuthRouter extends ApiRouter {
    get controller() {
        return CONTROLLER.AUTH
    }

    constructor(
        @inject(TYPES.INTERCEPTOR)
        protected interceptor: IInterceptor,
        @inject(TYPES.LOGGER)
        logger: ILogger,
        @inject(TYPES.ROUTER_VALIDATOR)
        validator: IRouterValidator,
    ) {
        super(logger, validator);
        this.stack.push(AuthRouter.prefix);
        this.log.info(this.path)

        this.signIn();
    }

    static get prefix() {
        return 'auth';
    }

    // @ts-ignore
    get id() {
        return Symbol.for('AUTH_ROUTER');
    }

    private signIn() {
        this.router.post(`${this.path}/sign-in`, this.validator.validate, this.interceptor.intercept(this.controller, `signIn`))
    }
}
