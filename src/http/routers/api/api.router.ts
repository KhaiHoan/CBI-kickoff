import { injectable } from "inversify";
import { BaseRouter, ILogger, IRouterValidator } from "../../../infrastructure";



@injectable()
export class ApiRouter extends BaseRouter {
    constructor(logger: ILogger, validator: IRouterValidator) {
        super(logger, validator);
        if (ApiRouter.prefix) {
            this.stack.push(ApiRouter.prefix);
        }
    }

    static get prefix() {
        return 'apis';
    }
}