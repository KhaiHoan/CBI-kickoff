import { TYPES } from "../const";
import { Http } from "../infrastructure";
import { singletonProvide } from "../infrastructure/ioc";


@singletonProvide(TYPES.HTTP)
export class ApiFurHttp extends Http {
    constructor() {
        super()
    }
}