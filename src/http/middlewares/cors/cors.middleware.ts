import cors from 'cors';
import { TYPES } from '../../../const';
import { BaseMiddleware } from '../../../infrastructure';
import { singletonProvide } from '../../../infrastructure/ioc';

@singletonProvide(TYPES.MIDDLEWARE)
export class CorsMiddleware extends BaseMiddleware {
    constructor() {
        super();
        this.middleware.use(cors());
    }

    get id() {
        return Symbol.for('CORS_MIDDLEWARE');
    }
}
