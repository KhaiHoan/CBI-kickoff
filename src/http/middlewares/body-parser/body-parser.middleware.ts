import bodyParser from "body-parser";
import { TYPES } from "../../../const";
import { BaseMiddleware } from "../../../infrastructure";
import { singletonProvide } from "../../../infrastructure/ioc";


@singletonProvide(TYPES.MIDDLEWARE)
export class BodyParserMiddleware extends BaseMiddleware {
    constructor() {
        super();
        this.middleware.use(bodyParser.json())
    }

    get id() {
        return Symbol.for('BODY_PARSER_MIDDLEWARE');
    }
}
