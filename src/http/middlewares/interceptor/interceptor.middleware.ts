import { RequestHandler, NextFunction, Request, Response } from "express";
import { inject } from "inversify";
import { container } from "../../..";
import { TYPES, NAMES } from "../../../const";
import { PostgresDatabase } from "../../../database";
import { IContextService, Context, KnexTransaction } from "../../../infrastructure";
import { singletonProvide, namedInject } from "../../../infrastructure/ioc";


export type ControllerResult = {
    httpCode?: number;
    headers?: { [key: string]: string };
    content: any;
};

export interface IInterceptor {
    intercept(controller: symbol, method: string): RequestHandler
}

@singletonProvide(TYPES.INTERCEPTOR)
export class Interceptor {

    constructor(
        @namedInject(TYPES.DATABASE, NAMES.POSTGRES)
        protected database: PostgresDatabase,

        @inject(TYPES.CONTEXT_SERVICE)
        protected contextService: IContextService
    ) {

    }

    public intercept(controller: symbol, method: string): RequestHandler {
        const interceptor: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
            let transaction: KnexTransaction = null;

            if (req.method !== 'GET') transaction = await this.database.connection.transaction();

            try {
                const ctx = await this.contextService.initRequestContext(req, transaction);
                container.rebind<Context>(TYPES.HTTP_CONTEXT).toConstantValue(ctx)

                const ctrl = container.getNamed<any>(TYPES.CONTROLLER, controller.toString());

                const result: ControllerResult = await ctrl[method](req);

                if (!!transaction && !transaction.isCompleted()) {
                    await transaction.commit()
                };

                return res.status(result.httpCode || 200).json(result.content);
            }
            catch (error) {
                if (!!transaction && !transaction.isCompleted()) {
                    await transaction.rollback();
                }

                console.log(error)

                return res.status(401).json({ error: error.message })
            }
        }

        return interceptor;
    }
}