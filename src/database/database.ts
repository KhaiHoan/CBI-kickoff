import knex, { Knex } from "knex";
import { API_DOMAIN, LOGGER, NAMES, TYPES } from "../const";
import { ITaskDomain, IUserDomain, IUserTaskDomain } from "../domains";
import { DatabaseModels, IConfiguration, IKnexModel, ILog, ILogger, KnexDatabaseConfig } from "../infrastructure";
import { inject, namedInject, singletonNamedProvide } from "../infrastructure/ioc";

export interface IDatabase {
    connection: Knex;
    dbModels: DatabaseModels;
    authenticate(): void;
}

@singletonNamedProvide(TYPES.DATABASE, NAMES.POSTGRES)
export class PostgresDatabase implements IDatabase {
    public connection: Knex;

    public dbModels: DatabaseModels;

    get id() {
        return NAMES.POSTGRES
    }

    get log(): ILog {
        const databaseLogger = Symbol.keyFor(LOGGER.INFRASTRUCTURE)
        const id = Symbol.keyFor(this.id)

        return this.logger.get(databaseLogger, id)
    }

    constructor(
        @inject(TYPES.CONFIG) protected config: IConfiguration,
        @inject(TYPES.LOGGER) protected logger: ILogger,

        @namedInject(TYPES.DATABASE, API_DOMAIN.USER)
        protected userModel: IKnexModel<IUserDomain>,

        @namedInject(TYPES.DATABASE, API_DOMAIN.TASK)
        protected taskModel: IKnexModel<ITaskDomain>,

        @namedInject(TYPES.DATABASE, API_DOMAIN.USER_TASK)
        protected userTaskModel: IKnexModel<IUserTaskDomain>,

    ) {
        try {
            this.authenticate();
        }
        catch (error) {
            this.log.error(error.message);
            process.exit();
        }
    }

    public authenticate(): void {
        const dbConfig: KnexDatabaseConfig = {
            ...this.config.get('postgres'),
        }

        this.connection = knex(dbConfig);
    }

    //     private initialModels() {
    //         const dbModels: DatabaseModels = {
    //             user: this.userModel.define(this.connection),
    //             task: this.taskModel.define(this.connection),
    //             userTask: this.userTaskModel.define(this.connection)
    //         }

    //         this.dbModels = dbModels
    //     }
}