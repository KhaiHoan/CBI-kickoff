import { API_DOMAIN, TABLE, TYPES } from "../../../const";
import { IUserDomain, IUserTaskDomain } from "../../../domains";
import { BaseKnexModel, IKnexModel } from "../../../infrastructure";
import { singletonNamedProvide } from "../../../infrastructure/ioc";

export interface IUserTaskKnexModel extends IKnexModel<IUserTaskDomain> { }

@singletonNamedProvide(TYPES.DATABASE, API_DOMAIN.USER_TASK)
export class UserTaskKnexModel extends BaseKnexModel<IUserTaskDomain> implements IUserTaskKnexModel {
    get tableName(): string {
        return TABLE.USER_TASK
    }
}