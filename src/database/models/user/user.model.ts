import { API_DOMAIN, TABLE, TYPES } from "../../../const";
import { IUserDomain } from "../../../domains";
import { BaseKnexModel, IKnexModel } from "../../../infrastructure";
import { singletonNamedProvide } from "../../../infrastructure/ioc";

export interface IUserKnexModel extends IKnexModel<IUserDomain> { }

@singletonNamedProvide(TYPES.DATABASE, API_DOMAIN.USER)
export class UserKnexModel extends BaseKnexModel<IUserDomain> implements IUserKnexModel {
    get tableName(): string {
        return TABLE.USER
    }
}