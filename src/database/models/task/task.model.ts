import { API_DOMAIN, TABLE, TYPES } from "../../../const";
import { ITaskDomain, IUserDomain } from "../../../domains";
import { BaseKnexModel, IKnexModel } from "../../../infrastructure";
import { singletonNamedProvide } from "../../../infrastructure/ioc";

export interface ITaskKnexModel extends IKnexModel<IUserDomain> { }

@singletonNamedProvide(TYPES.DATABASE, API_DOMAIN.TASK)
export class TaskKnexModel extends BaseKnexModel<ITaskDomain> implements ITaskKnexModel {
    get tableName(): string {
        return TABLE.TASK
    }
}