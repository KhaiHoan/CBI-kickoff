import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../const";
import { IUserTaskDomain } from "../../domains";
import { BaseDatabMapper, IDataMapper, IPasswordHelper } from "../../infrastructure";
import { singletonNamedProvide } from "../../infrastructure/ioc";

export interface IUserTaskMapper extends IDataMapper<IUserTaskDomain> { }

@singletonNamedProvide(TYPES.MAPPER, API_DOMAIN.USER_TASK)
export class UserTaskMapper extends BaseDatabMapper<IUserTaskDomain> implements IUserTaskMapper {
    @inject(TYPES.PASSWORD_HELPER) protected passwordHelper: IPasswordHelper;

    protected entityType = API_DOMAIN.USER_TASK;

    fromEntityToDatabase(params: Record<string, any>): Record<string, any> {
        return {
            id: params.id,
            user_id: params.userId,
            task_id: params.taskId,
            status: params.status,
            created_at: params.createdAt,
            created_by: params.createdBy,
            updated_at: params.updatedAt,
            updated_by: params.updatedBy
        }
    }

    fromDatabaseToEntity(params: Record<string, any>): Record<string, any> {
        return {
            id: params.id,
            userId: params.user_id,
            taskId: params.task_id,
            status: params.status,
            createdAt: params.created_at,
            createdBy: params.created_by,
            updatedAt: params.updated_at,
            updatedBy: params.updated_by
        }
    }
}