import { TYPES, API_DOMAIN, NAMES, TABLE } from "../../const";
import { IDatabase } from "../../database";
import { IUserTaskDomain } from "../../domains";
import { KnexDatabaseModel, KnexTransaction } from "../../infrastructure";
import { singletonNamedProvide, namedInject } from "../../infrastructure/ioc";
import { IRepository, BaseKnexRepository } from "../../infrastructure/repository/repository/base.repository";
import { IUserTaskMapper } from "./user-task.mapper";



export interface IUserTaskRepository extends IRepository<IUserTaskDomain> {
    findByUserId(id: string): Promise<IUserTaskDomain[]>;
    findByTaskId(id: string): Promise<IUserTaskDomain[]>;
    deleteByTaskId(id: string, transaction?: KnexTransaction): Promise<void>;
    countByUserId(id: string): Promise<number>;
}

@singletonNamedProvide(TYPES.REPOSITORY, API_DOMAIN.USER_TASK)
export class UserTaskRepository extends BaseKnexRepository<IUserTaskDomain> implements IUserTaskRepository {
    @namedInject(TYPES.MAPPER, API_DOMAIN.USER_TASK)
    protected mapper: IUserTaskMapper;

    protected tableName: string = TABLE.USER_TASK

    constructor(
        @namedInject(TYPES.DATABASE, NAMES.POSTGRES)
        protected database: IDatabase
    ) {
        super(database)
    }

    public async findByUserId(id: string): Promise<IUserTaskDomain[]> {
        const docs = await this.model.where({ user_id: id }).select('*')

        return docs.length > 0 ? docs.map(doc => this.mapper.toEntity(doc)) : [];
    }

    public async findByTaskId(id: string): Promise<IUserTaskDomain[]> {
        const docs = await this.model.where({ task_id: id }).select('*')

        return docs.length > 0 ? docs.map(doc => this.mapper.toEntity(doc)) : [];
    }

    public async deleteByTaskId(id: string, transaction?: KnexTransaction): Promise<void> {
        const query = this.model.where({ task_id: id }).delete();

        if (transaction) query.transacting(transaction);

        await Promise.resolve(query);

        return;
    }

    public async countByUserId(id: string): Promise<number> {
        const records: [{ count: string }] = await this.model.count().where({ user_id: id });

        return Number.parseInt(records[0].count);
    }
}