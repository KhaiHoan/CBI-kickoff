import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../const";
import { IUserDomain, UserDomain } from "../../domains";
import { IDataMapper, BaseDatabMapper, IPasswordHelper } from "../../infrastructure";
import { singletonNamedProvide } from "../../infrastructure/ioc";


export interface IUserMapper extends IDataMapper<IUserDomain> { }

@singletonNamedProvide(TYPES.MAPPER, API_DOMAIN.USER)
export class UserMapper extends BaseDatabMapper<IUserDomain> implements IDataMapper<IUserDomain> {
    @inject(TYPES.PASSWORD_HELPER) protected passwordHelper: IPasswordHelper;

    protected entityType = API_DOMAIN.USER;

    fromEntityToDatabase(params: Record<string, any>): Record<string, any> {
        return {
            id: params.id,
            first_name: params.firstName,
            last_name: params.lastName,
            email: params.email,
            password: params.password,
            status: params.status,
            created_at: params.createdAt,
            created_by: params.createdBy,
            updated_at: params.updatedAt,
            updated_by: params.updatedBy
        }
    }

    fromDatabaseToEntity(params: Record<string, any>): Record<string, any> {
        return {
            id: params.id,
            firstName: params.first_name,
            lastName: params.last_name,
            email: params.email,
            password: params.password,
            status: params.status,
            createdAt: params.created_at,
            createdBy: params.created_by,
            updatedAt: params.updated_at,
            updatedBy: params.updated_by
        }
    }

    toDatabase(entity: IUserDomain): any {
        if (entity.password) {
            entity.password = this.passwordHelper.encrypt(entity.password)
        }

        return super.toDatabase(entity)
    }
}