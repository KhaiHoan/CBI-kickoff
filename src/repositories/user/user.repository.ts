import { TYPES, API_DOMAIN, NAMES, TABLE } from "../../const";
import { IDatabase } from "../../database";
import { IUserDomain } from "../../domains";
import { KnexDatabaseModel } from "../../infrastructure";
import { singletonNamedProvide, namedInject } from "../../infrastructure/ioc";
import { IRepository, BaseKnexRepository } from "../../infrastructure/repository/repository/base.repository";
import { IUserMapper } from "./user.mapper";

export interface IUserRepository extends IRepository<IUserDomain> {
    findByEmail(email: string, select?: string[]): Promise<IUserDomain>
}

@singletonNamedProvide(TYPES.REPOSITORY, API_DOMAIN.USER)
export class UserRepository extends BaseKnexRepository<IUserDomain> implements IUserRepository {
    @namedInject(TYPES.MAPPER, API_DOMAIN.USER)
    protected mapper: IUserMapper;

    protected tableName: string = TABLE.USER;

    constructor(
        @namedInject(TYPES.DATABASE, NAMES.POSTGRES)
        protected database: IDatabase
    ) {
        super(database)
    }

    public async findByEmail(email: string): Promise<IUserDomain> {
        const doc = await this.model.select('*').where({ email });

        return doc.length > 0 ? this.mapper.toEntity(doc[0]) : null;

    }
}