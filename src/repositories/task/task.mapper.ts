import { inject } from "inversify";
import { TYPES, API_DOMAIN } from "../../const";
import { ITaskDomain } from "../../domains/task";
import { BaseDatabMapper, IDataMapper, IPasswordHelper } from "../../infrastructure";
import { singletonNamedProvide } from "../../infrastructure/ioc";

export interface ITaskMapper extends IDataMapper<ITaskDomain> { }

@singletonNamedProvide(TYPES.MAPPER, API_DOMAIN.TASK)
export class TaskMapper extends BaseDatabMapper<ITaskDomain> implements ITaskMapper {
    @inject(TYPES.PASSWORD_HELPER) protected passwordHelper: IPasswordHelper;

    protected entityType = API_DOMAIN.TASK;

    fromEntityToDatabase(params: Record<string, any>): Record<string, any> {
        return {
            id: params.id,
            title: params.title,
            start_time: params.startTime,
            end_time: params.endTime,
            description: params.description,
            status: params.status,
            created_at: params.createdAt,
            created_by: params.createdBy,
            updated_at: params.updatedAt,
            updated_by: params.updatedBy
        }
    }

    fromDatabaseToEntity(params: Record<string, any>): Record<string, any> {
        return {
            id: params.id,
            title: params.title,
            startTime: params.start_time,
            endTime: params.end_time,
            description: params.description,
            status: params.status,
            createdAt: params.created_at,
            createdBy: params.created_by,
            updatedAt: params.updated_at,
            updatedBy: params.updated_by
        }
    }
}