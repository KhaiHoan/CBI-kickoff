import { TYPES, API_DOMAIN, NAMES, TABLE } from "../../const";
import { IDatabase } from "../../database";
import { ITaskDomain, IUserDomain } from "../../domains";
import { KnexDatabaseModel } from "../../infrastructure";
import { singletonNamedProvide, namedInject } from "../../infrastructure/ioc";
import { IRepository, BaseKnexRepository } from "../../infrastructure/repository/repository/base.repository";
import { ITaskMapper } from "./task.mapper";



export interface ITaskRepository extends IRepository<ITaskDomain> {
    findByUserId(id: string): Promise<ITaskDomain>
}

@singletonNamedProvide(TYPES.REPOSITORY, API_DOMAIN.TASK)
export class TaskRepository extends BaseKnexRepository<ITaskDomain> implements ITaskRepository {
    @namedInject(TYPES.MAPPER, API_DOMAIN.TASK)
    protected mapper: ITaskMapper;

    protected tableName: string = TABLE.TASK;

    constructor(
        @namedInject(TYPES.DATABASE, NAMES.POSTGRES)
        protected database: IDatabase
    ) {
        super(database)

    }

    public async findByUserId(id: string): Promise<ITaskDomain> {

        const doc = await this.model.where({ id }).select('*')

        return doc.length > 0 ? this.mapper.toEntity(doc[0]) : undefined;
    }
}