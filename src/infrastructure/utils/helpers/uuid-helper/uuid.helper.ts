import { v4 as uuidv4, V4Options as UUIdV4Options, version as uuidValidateVersion, validate as uuidValidateFormat } from 'uuid';
import { TYPES } from '../../../../const';
import { singletonProvide } from '../../../ioc';

export interface IUUIdHelper {
    generate(options?: UUIdV4Options): string;
    validate(uuid: string, version?: number): boolean;
}

@singletonProvide(TYPES.UUID_HELPER)
export class UUIdHelper implements IUUIdHelper {
    private readonly version: number = 4;

    generate(options?: UUIdV4Options): string {
        return uuidv4(options);
    }

    validate(uuid: string, version?: number): boolean {
        let ver = this.version;

        if (version) ver = version;

        return uuidValidateFormat(uuid) && uuidValidateVersion(uuid) === ver;
    }
}