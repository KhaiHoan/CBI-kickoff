import { Request } from 'express';
import { API_DOMAIN, TYPES } from '../../../const';
import { IUserDomain } from '../../../domains';
import { IUserRepository } from '../../../repositories';
import { KnexTransaction } from '../../database';
import { IErrorFactory } from '../../error';
import { inject, namedInject, singletonProvide } from '../../ioc';
import { IJwtHelper, IObjectHelper } from '../../utils';
import { Context } from './context-entity';

export interface IContextService {
    initRequestContext(req: Request, transaction?: any): Promise<Context>;
}

@singletonProvide(TYPES.CONTEXT_SERVICE)
export class ContextService {
    @inject(TYPES.OBJECT_HELPER)
    protected objectHelper: IObjectHelper;

    @inject(TYPES.JWT_HELPER)
    protected jwtHelper: IJwtHelper;


    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @namedInject(TYPES.REPOSITORY, API_DOMAIN.USER)
    protected userRepository: IUserRepository

    public async initRequestContext(req: Request, transaction?: KnexTransaction): Promise<Context> {
        let user: IUserDomain = null;

        if (req.headers.authorization) {
            const payload = this.jwtHelper.verify(req.headers.authorization);
            user = await this.userRepository.findById(payload.id)

            if (!user) {
                throw this.errorFactory.unauthorizedError(`Invalid token`)
            }
        }

        const ctx = new Context(
            this.objectHelper.pick(req, ['params', 'query', 'body', 'header', 'protocol', 'originalUrl']),
            user,
            transaction
        );

        return ctx;
    }
}