import { Request } from "express";
import { TYPES } from "../../../const";
import { IUserDomain } from "../../../domains";
import { KnexTransaction } from "../../database";
import { inject, namedInject } from "../../ioc";
import { IObjectHelper } from "../../utils";

export class Context {
  public transaction?: KnexTransaction;
  public timestamp: number;
  public protocol: string;
  public originalUrl: string;
  public token: string;
  public user: IUserDomain

  constructor(req: Request, user?: IUserDomain, transaction?: KnexTransaction) {

    if (transaction) { this.transaction = transaction }

    if (user) { this.user = user }

    this.timestamp = Date.now();
    this.protocol = req.protocol;
    this.originalUrl = req.originalUrl;
  }
}
