import { TYPES } from "../../../const";
import { IEntityFactory } from "../../../domains/domain.factory";
import { IDomain } from "../../base/domain";
import { inject, injectable } from '../../ioc';
import { IObjectHelper } from "../../utils";

export interface IDataMapper<D> {
    fromDatabaseToEntity(params: Record<string, any>): Record<string, any>
    fromEntityToDatabase(params: Record<string, any>): Record<string, any>

    toEntity(data: any): D;
    toDatabase(entity: D): any;
}

@injectable()
export abstract class BaseDatabMapper<D extends IDomain> implements IDataMapper<D> {
    @inject(TYPES.ENTITY_FACTORY)
    protected entityFactory: IEntityFactory;

    @inject(TYPES.OBJECT_HELPER)
    protected objectHelper: IObjectHelper;

    protected abstract entityType: symbol;

    abstract fromDatabaseToEntity(params: Record<string, any>): Record<string, any>
    abstract fromEntityToDatabase(params: Record<string, any>): Record<string, any>

    public toEntity(data: any): D {
        return <D>this.entityFactory.create(this.entityType.toString(), this.fromDatabaseToEntity(data))
    }

    public toDatabase(entity: D): any {
        return this.fromEntityToDatabase(this.objectHelper.omitByUndefined(entity))
    }
}