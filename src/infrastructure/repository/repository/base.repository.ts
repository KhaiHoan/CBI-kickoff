import { LOGGER, NAMES, TYPES } from "../../../const";
import { IDatabase } from "../../../database";
import { IDomain } from "../../base";
import { KnexDatabaseModel, KnexTransaction } from "../../database";
import { IErrorFactory } from "../../error";
import { IArrayHelper, IUUIdHelper } from "../../utils";
import { IDataMapper } from "../mapper";
import { inject, injectable } from '../../ioc';

export type Criteria = {
    select?: string[],
    filter?: { key: string, value: any }[],
    order?: { column: string, order: string }[],
    limit?: number,
    offset?: number,
}

export interface IRepository<D extends IDomain> {
    model: KnexDatabaseModel;

    findById(id: string): Promise<D>;
    updateById(id: string, payload: D, transaction?: KnexTransaction): Promise<D>;
    create(payload: D, transaction?: KnexTransaction): Promise<D>;
    deleteById(id: string, transaction?: KnexTransaction): Promise<void>;
    paginate(criteria: Criteria): Promise<D[]>;
}

@injectable()
export abstract class BaseKnexRepository<D extends IDomain> implements IRepository<D> {
    get model(): KnexDatabaseModel {
        return this.postgresDatabase.connection<D>(this.tableName);
    }

    protected abstract tableName: string;
    protected abstract mapper: IDataMapper<D>;

    @inject(TYPES.ERROR_FACTORY)
    protected errorFactory: IErrorFactory;

    @inject(TYPES.ARRAY_HELPER)
    protected arrayHelper: IArrayHelper;

    @inject(TYPES.UUID_HELPER)
    protected uuidHelper: IUUIdHelper;

    constructor(
        protected postgresDatabase: IDatabase
    ) { }

    async findById(id: string): Promise<D> {
        const data = await this.model.select<D>('*').where({ id });

        return data.length > 0 ? this.mapper.toEntity(data[0]) : null;
    }

    async updateById(id: string, payload: D, transaction?: KnexTransaction): Promise<D> {
        const query = this.model.update<D>(this.mapper.toDatabase(payload)).where({ id });

        if (transaction) { query.transacting(transaction) }

        const data = await Promise.resolve(query.returning('*'));

        return this.mapper.toEntity(data[0]);
    }

    async create(payload: D, transaction?: KnexTransaction): Promise<D> {
        const dao = this.mapper.toDatabase({ ...payload, id: this.uuidHelper.generate() });

        const query = this.model.insert<D>(dao);

        if (transaction) {
            query.transacting(transaction)
        }

        const data = await Promise.resolve(query.returning('*'));

        return this.mapper.toEntity(data[0]);
    }

    async deleteById(id: string, transaction?: KnexTransaction): Promise<void> {
        const query = this.model.where({ id }).delete();

        if (transaction) { query.transacting(transaction) }

        await Promise.resolve(query);

        return;
    }

    async paginate(criteria: Criteria): Promise<D[]> {
        let query = this.model.select('*');

        if (criteria.select) {
            query = this.model.select(criteria.select)
        }

        if (criteria.order) {
            query = query.orderBy(criteria.order)
        }

        if (criteria.limit) {
            query = query.limit(criteria.limit)
        }

        if (criteria.offset) {
            query = query.offset(criteria.offset)
        }

        if (criteria.filter.length > 0) {
            criteria.filter.forEach(condition => query.where({ [condition.key]: condition.value }))
        }

        // const forLog = query;

        // console.log(forLog.toSQL().toNative)

        const docs = await Promise.resolve(query) as D[];

        return docs.map(doc => this.mapper.toEntity(doc));
    }
}

