import { injectable } from '../../ioc';
import { IDomain } from '../../base/domain';

import { Knex } from 'knex';


export interface IKnexModel<IDomain> {
    define(connection: Knex): Knex.QueryBuilder;
}

@injectable()
export abstract class BaseKnexModel<I extends IDomain> implements IKnexModel<I> {
    public model: Knex.QueryBuilder;

    abstract tableName: string;

    define(connection: Knex): Knex.QueryBuilder {
        this.model = connection<I>(this.tableName);

        return this.model;
    }
}