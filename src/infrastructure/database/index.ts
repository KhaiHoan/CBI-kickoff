export type DatabaseModels = {
    user?: KnexDatabaseModel,
    task?: KnexDatabaseModel,
    userTask?: KnexDatabaseModel
};

import { Knex } from 'knex';
import { IDomain } from '../base';

export type KnexDatabaseConfig = Knex.Config;

export type KnexDatabaseModel = Knex.QueryBuilder

export type KnexQueryResult<I extends IDomain> = Knex.QueryBuilder<I, I[]>

export type KnexTransaction = Knex.Transaction

export * from './model';
