// const database = container.getNamed<IDatabase>(TYPES.DATABASE, NAMES.POSTGRES)

import { USER_STATUSES, TYPES, API_DOMAIN } from "../../src/const";
import { UserDomain } from "../../src/domains";
import { IUserRepository } from "../../src/repositories";

import { container } from '../index'

let userRepo: IUserRepository = null

const mockerUserDomain1 = new UserDomain({
    firstName: 'One',
    lastName: 'Mock',
    email: 'mock.one@gmail.com',
    password: 'mockone123',
    status: USER_STATUSES.ACTIVE,
    createdAt: new Date(),
    createdBy: null,
    updatedAt: new Date(),
    updatedBy: null
}).json()

const wait = (ms: number) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};

beforeAll(async () => {
    await wait(100);

    userRepo = container.getNamed<IUserRepository>(TYPES.REPOSITORY, API_DOMAIN.USER)
});

async function cleanDataIfExisted(): Promise<void> {
    const isExistedMock1 = await userRepo.findByEmail(mockerUserDomain1.email);

    if (isExistedMock1) await userRepo.deleteById(isExistedMock1.id)
}

beforeEach(async () => {
    await cleanDataIfExisted();
});

describe('Create user', () => {
    test('Create user mock 1 success', async () => {
        const user1 = await userRepo.create(mockerUserDomain1);
        expect(user1).toHaveBeenCalledTimes(1)

        expect(typeof user1.id).toBe("string")

        expect(typeof user1.lastName).toBe("string")
        expect(typeof user1.firstName).toBe("string")
        expect(typeof user1.status).toBe("number")
    });
});
