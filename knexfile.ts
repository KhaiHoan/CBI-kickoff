require('dotenv').config()

module.exports = {
    development: {
        client: process.env.POSTGRES_CLIENT,
        connection: {
            host: process.env.POSTGRES_HOST,
            port: Number.parseInt(process.env.POSTGRES_PORT),
            user: process.env.POSTGRES_USERNAME,
            password: process.env.POSTGRES_PASSWORD,
            database: process.env.POSTGRES_DATABASE,
        },
        migrations: {
            directory: __dirname + '/database/migrations'
        },
        seeds: {
            directory: __dirname + '/database/seeds'
        },
    }
};